; made by debiankaios, License:
; MIT License
;
; Copyright (c) [2022] [debiankaios]
;
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.
;
; This isn't final!


"and" @keyword
"false" @keyword
"local" @keyword
"then" @keyword
"break" @keyword
"for" @keyword
"nil" @keyword
"true" @keyword
"do" @keyword
"function" @keyword
"not" @keyword
"until" @keyword
"else" @keyword
"goto" @keyword
"or" @keyword
"while" @keyword
"elseif" @keyword
"if" @keyword
"repeat" @keyword
"end" @keyword
"in" @keyword
"return" @keyword

"+" @operator
"-" @operator
"*" @operator
"/" @operator
"%" @operator
"^" @operator
"#" @operator
"&" @operator
"~" @operator
"|" @operator
"<<" @operator
">>" @operator
"//" @operator
"==" @operator
"~=" @operator
"<=" @operator
">=" @operator
"<" @operator
">" @operator
"=" @operator

"::" @operator
";" @operator
":" @operator
"," @operator
"." @operator
".." @operator
"..." @operator


[(nil) (true) (false)] @constant.builtin

((call
  function: (identifier) @function.builtin)
 (#match?
   @function.builtin
   "^(print|tonumber|tostring)$"))
